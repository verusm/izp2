/*
* Nazev souboru: proj2.c
* Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz
* Datum: 18. 11. 2009
* Projekt: Iteracni vypocty, projekt cislo 2 pro predmet IZP
* Popis programu: Program pomoci iteracnich algoritmu zpracuje libovolne dlouhou posloupnost ciselnych hodnot typu double
                ze standardniho vstupu podle zadanych parametru.
                Vystupem programu je stejne dlouha ciselna posloupnost vysledku, jako je posloupnost vstupni.
*/

/** Knihovny potrebne pro beh programu.**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

/** Pouzivana konstanta.**/
const double ln10 = 2.302585093;                   //ln 10

/** Funkce, ktera nam po zavolani vypise napovedu pro tento program.**/
void napoveda(void)
{
    printf("Program Iteracni vypocty (2009)\n"
          "Autor: Vera Mullerova \n"
          "**************************** \n"
          " Program zpracuje podle zadanych parametru pomoci iteracnich vypoctu libovolne\n"
          " dlouho posloupnost ciselnych hodnot typu double zadanych na standardnim vstupu.\n"
          " Vysledkem je vypis stejne dlouhe ciselne posloupnosti, jako je posloupnost vstupni.\n"
          " \n"
          "Popis parametru: \n"
          "     -h                  zobrazi tuto napovedu \n"
          "     --powax sigdig a    vypocte obecnou mocninu o zakladu a s presnosti sigdig\n"
          "     --logax sigdig a    vypocte obecny logaritmus o zakladu a s presnosti sigdig\n"
          "     --hm                vypise stejne dlouhou posloupnost harmonickych prumeru \n"
          "                         jako je posloupnost vstupni\n"
          "     --stddev            vypise stejne dlouhou poslopunost smerodatne odchylky \n"
          "                         jako je posloupnost vstupnich dat\n"
    );
}

/** Vyctovy typ chyby, ve kterem jsou definovane chyby, ktere se mohou v programu objevit. **/
enum chyby
{
    EROK, // bez chyby
    ERPARAM, // spatny parametr
    ERSIG, // spatne zadana presnost
    ERSIGMAX, // moc velka presnost
    ERSIGMIN, // presnost je mensi nebo rovno 0
    ERZAKLAD, // spatne zadane a
    ERNEVIM // neznama chyba
};

/** Definice pole s chybovymi hlaskami. **/
const char *CHYBY[] =
{
    "Vse v poradku.",
    "Spatne zadany parametr.",
    "Spatne zadana presnost.",
    "Zadana presnost je moc velka. Bude se pracovat s co nejvetsi presnosti to jde.",
    "Zadana presnost je prilis mala.",
    "Spatne zadany treti vstupni parametr (zaklad).",
    "Neznama chyba."
};

/**
* Funkce vypisujici chybove hlasky.
* @param chyba Chyba z vyctu chyb.
**/
void vypisChybu(int chyba)
{
    if (chyba < EROK || chyba >= ERNEVIM) //pokud neni chybova hlaska mezi EROK a ERNEVIM, tak se stalo neco spatne
        chyba = ERNEVIM; //a vypise se mi chybova hlaska ERNEVIM
    fprintf(stderr, "%s \n", CHYBY[chyba]); //jinak se vypise prislusna chyba, ktera v programu opravdu nastala
}

/**
* Funkce, ktera overi zadanou presnost a prevede ji na typ double.
* @param argv Pole textovych retezcu s argumenty.
* @param chyba Preda chybove hlaseni.
**/
double overeniPresnost(char *argv[], int *chyba)
{
    char *chyb, *s;
    s = argv[2];
    double sigdig = strtol(s, &chyb, 10);
    if (chyb == s) //uz 1. znak z 2. parametru nebylo cislo
    {
        sigdig = NAN;
        *chyba = ERSIG;
    }
    if (*chyb != '\n' && *chyb != 0) //nejaky dalsi znak z 2. parametru neni cislice
    {
        sigdig = NAN;
        *chyba = ERSIG;
    }
    if (sigdig <= 0) // presnost je mensi nebo rovna nulu - vypisu chybovou hlasku, nastavim sigdig na NAN a vsechny vysledky jsou tedy take NAN
    {
        *chyba = ERSIGMIN; // pomoci ukazatele predame chybu do hlavni funkce, kde se dale zpracovava
        sigdig = NAN;
    }
    else if (sigdig > DBL_DIG)
    {
        *chyba = ERSIGMAX;
        sigdig = DBL_DIG;
    }
    else
    {}
    return sigdig;
}

/**
* Funkce, ktera prevede zadane cislo tak, aby bylo mensi nez 1 a vetsi nez O.1.
* Zaroven predava 10 na kolikatou je cislo nasobeno, abychom mohli vypocitat konecny vysledek.
* Tuto fuknci pouzivam proto, abych pak samotny vypocet provadela v intervalu od 0.1 do 1, kde logaritmus konverguje nejefektivneji.
* @param cislo Cislo, ktere posouvam do intervalu od 0.1 do 1.
* @param n Ukazatel na pocet o kolik radu bylo cislo posunuto.
**/
double rozlozeni(double cislo, int *n)
{
    double hotovo = -1;
    int pocet = *n;

    if (cislo >= 1)
    {
        while (hotovo < 0)
        {
            cislo = cislo / 10;
            hotovo = 1 - cislo;
            pocet ++;
        }
        *n = pocet; // pomoci ukazatele predavam pocet o kolik radu bylo puvodni cislo posunuto
    }
    else if (cislo < 0.1)
    {
        while (hotovo < 1)
        {
            if (cislo < 0.1)
            {
                cislo = cislo * 10;
                hotovo = 1 - cislo;
                pocet ++;
            }
            else
            {
                *n = (-1)*pocet;
                return cislo;
            }
        }
        *n = (-1)*pocet; // pomoci ukazatele predavam pocet o kolik radu bylo puvodni cislo posunuto
    }
    else
    {}

    return cislo; // predavam cislo, ktere je z intervalu od 0 do 1
}

/**
* Funkce, ktera vypocita prirozeny logaritmus zadaneho cisla se zadanou presnosti.
* @param y Cislo, ktereho pocitam logaritmus.
* @param presnost Zadana presnost jako 1(a pocet nul) tak, abychom timto cislem mohli delit a tim overovali presnost vysledku.
* @param pocet Pocet radu, o ktere bylo puvodni cislo prevedeno na cislo z intervalu od 0.1 do 1.
**/
double lny(double y, int presnost, int pocet)
{
    double lny = 0;
    double predvysledek = 0;
    double hotovo = 1;
    double vysledek = 0;
    int i = 1;

    while (hotovo >= 1) // dokud je presnost mala, probiha cyklus
    {
        lny = lny + pow((y-1), i)/ (i*pow((y+1), i)); // k prubeznemu souctu pripocitavam nasledujici podil Taylorovy rady
        predvysledek = vysledek;
        vysledek = 2*lny + (pocet*ln10);
        hotovo = fabs(predvysledek - vysledek); // porovnavam vysledek s predchozim vysledkem
        hotovo = hotovo * presnost; // zjistuji, (z predchoziho porovnani) zda uz je dosazeno pozadovane presnosti
        i = i+2; // pomocna, diky ktere mohu vypocitat postupne Taylorovu radu pro prirozeny logaritmus
    }
    return vysledek;
}

/**
* Funkce pocitajici obecnou mocninu cele casti cisla.
* @param cela Cela cast z cisla zadaneho na standardnim vstupu.
* @param a Cislo typu double, ktere urcuje jaky je zaklad mocniny.
**/
double mocninaCela(double cela, double a)
{
    double vysCela = 1;
    if (cela > 0.0) // pokud je cela cast kladna, vypocitam postupnym nasobenim pozadovanou mocninu
    {
        vysCela = a;
        for (int i=1; i<cela; i++)
            vysCela = vysCela * a;
    }
    else if (cela < 0.0) // pokud je cela cast zaporna, musim pocitat jinak
    {
        vysCela = a;
        for (int i=1; i<(fabs(cela)); i++)
            vysCela = vysCela * a;
        vysCela = 1/vysCela; // vzorec pro vypocet zaporne mocniny (prevracena hodnota)
    }
    else
    {}
    return vysCela;
}

/**
* Funkce pocitajici obecnou mocninu desetinne casti cisla.
* @param cela Desetinna cast z cisla zadaneho na standardnim vstupu.
* @param presnost Zadana presnost, se kterou se ma pocitat.
* @param lna Mezi vysledek potrebny pri vypoctu a vypocinany pomoci funkce lny.
**/
double mocninaDesetinna(double desetinna, double presnost, double lna)
{
    int faktorial = 1;
    int pocet = 1;
    double zlomek = 0;
    double hotovo = 1;
    double predvysledek = 0;
    double vysledek = 1;

    if (desetinna != 0.0) // pokud neni desetinna cast nulova, tak vypocitavam pozadovanou mocninu
    {
        while (hotovo >=1) // dokud neni splnena zadana presnost, tak vypocitavam mocninu pomoci Taylorova rozvoje
        {
            faktorial = faktorial * pocet;
            zlomek = zlomek + (pow(desetinna, pocet)*pow(lna, pocet))/ faktorial;
            predvysledek = vysledek;
            vysledek = 1 + zlomek;
            hotovo = fabs(vysledek - predvysledek);
            hotovo = hotovo * presnost;
            pocet ++; // pomocna, kterou pouzivam ve vzorci Taylorova rozvoje
        }
    }
    else
    {}
    return vysledek;
}

/**
* Funkce pocitajici obecnou mocninu o zadanem zakladu se zadanou presnosti.
* Do teto funkce predavame jiz zpracovane promenne, ktere jsou pozadovaneho typu a z pozadovaneho definicniho oboru.
* @param a Cislo typu double, ktere urcuje jaky je zaklad mocniny.
* @param sigdig Cislo typu int, ktere urcuje presnost vypoctu.
* @param cislo Cislo typu double, ktere rika, o jakou jde mocninu.
**/
double powax_(double a, int sigdig, double cislo)
{
    double lna = 0;
    double presnost = 1;
    double vysledek = 1;
    double cela = 0;
    double desetinna = modf(cislo, &cela); // rozdeleni cisla na celou a desetinnou cast
    // v promenne desetinna je ulozena desetinna cast a v promenne cela cast cela

    for (int j=1; j<=sigdig; j++) // prevedeni sigdig na cislo presnost, kterym budu moci delit a tim kontrolovat presnost vypoctu
    {    presnost = presnost * 10;}

    // overeni ruznych vyjimecnych situaci, ve kterych jsou hned jasne vysledky
    if (a == 0 && cislo != 0)
        return vysledek = 0;
    if (cislo == 0 && a != 0)
        return vysledek = 1;

    int na = 0;
    double zaklad;
    zaklad = rozlozeni(a, &na);// do a se mi zapise promenna a vydelena tak, aby byla mensi nez 1 a vetsi nez 0.1
                        // do promenne na se mi zapise o kolik nul se musela hodnota a posunout, aby byla z intervalu (0.1;1)
    lna = lny(zaklad, presnost, na); // pomocny vypocet, ktery budu potrebovat v dalsim kroku vypoctu
    vysledek = mocninaDesetinna(desetinna, presnost, lna);
    double vysCela = mocninaCela(cela, a);
    vysledek = vysledek * vysCela; // spojeni vysledku vypoctu cele a desetinne casti zadaneho cisla
    return vysledek;
}

/**
* Obalovaci funkce pro funkci powax_.
* Funkce, ktera prevadi a overuje zadane parametry a cislo tak,
* aby byly samotne fuknci pro vypocet obecne mocniny predany pouze takove hodnoty,
* se kterymi bude schopna standardne pocitat.
* @param argv Pole textovych retezcu s argumenty.
* @param cislo Cislo zadane na standardnim vstupu.
* @param chyba Ukazatel na chybu.
**/
double powax(char *argv[], double cislo, int *chyba)
{
    double vysledek = 0;
    char *chyb, *s;
    // prevedeni zadanych parametru na typ double
    // pokud parametry nejsou cisla (nejdou prevest na typ double), nastavim je na hodnotu NAN
    int chybaPresnost = EROK;
    double sigdig = overeniPresnost(argv, &chybaPresnost);
    *chyba = chybaPresnost;

    s = argv[3];
    double zaklad = strtod(s, &chyb);
    if (chyb == s) //uz 1. znak z 2. parametru nebylo cislo
    {
        zaklad = NAN;
        *chyba = ERZAKLAD;
    }
    if (*chyb != '\n' && *chyb != 0) //nejaky dalsi znak z 2. parametru neni cislice
    {
        zaklad = NAN;
        *chyba = ERZAKLAD;
    }
    if (zaklad < 0)  // pokud je zaklad mensi nez 0, nastavime ho na hodnotu NAN
        zaklad = NAN;

    vysledek = powax_(zaklad, sigdig, cislo);
        // do promenne vysledek zapisu navratovou hodnotu z funkce powax_, ktera mi vypocita pozadovanou mocninu
    return vysledek;
}

/**
* Funkce pocitajici samotny logaritmus o zadanem zakladu se zadanou presnosti.
* Do teto funkce predavame jiz zpracovane promenne, ktere jsou pozadovaneho typu.
* @param zaklad Cislo typu double, ktere urcuje jaky je zaklad logaritmu.
* @param sigdig Cislo typu int, ktere urcuje presnost vypoctu.
* @param cislo Cislo typu double, ktere budeme logaritmovat.
**/
double logax_(double zaklad, int sigdig, double cislo)
{
    double vysledek;
    double x;
    double a;
    int nx = 0, na = 0;
    int presnost = 1;
    double sumx, suma;

    for (int i=1; i<=sigdig; i++) // presnost si prepocitam na cislo, kterym budu delit (cislo o tolikati nulach, jaka ma byt presnost)
        presnost = presnost * 10;

    if (zaklad == cislo) // overeni specialnich pripadu u logaritmu
        return vysledek = 1;
    else if (cislo == 1)
        return vysledek = 0;
    else
    {
        x = rozlozeni(cislo, &nx); // do x se mi zapise cislo vydelene tak, aby bylo mensi nez 1
                               // do nx se mi zapise o kolik nul se muselo cislo posunout, aby bylo mensi nez 1
        a = rozlozeni(zaklad, &na); // do a se mi zapise zaklad vydeleny tak, aby byl mensi nez 1
                               // do na se mi zapise o kolik nul se musel zaklad posunout, aby byl mensi nez 1

        sumx = lny(x, presnost, nx); // do sumx se mi zapise vysledek lnx
        suma = lny(a, presnost, na); // do suma se mi zapise vysledek lna

        vysledek = sumx/suma; // konecny vysledek
        return vysledek;
    }
}

/**
* Obalovaci funkce pro funkci logax_.
* Funkce, ktera prevadi a overuje zadane parametry a cislo tak,
* aby byly samotne funkci pro vypocet logaritmu predany pouze hodnoty takoveho typu,
* se kterymi bude schopna pocitat.
* @param argv Pole textovych retezcu s argumenty.
* @param cislo Cislo zadane na standardnim vstupu.
* @param chyba Ukazatel na chybu.
**/
double logax(char *argv[], double cislo, int *chyba)
{
    char *chyb, *s;
    double vysledek = 0;
    int chybaPresnost = EROK;
    double sigdig = overeniPresnost(argv, &chybaPresnost);
    *chyba = chybaPresnost;
    if (sigdig == NAN)
        return vysledek;

    s = argv[3];
    double zaklad = strtod(s, &chyb);
    if (chyb == s) //uz 1. znak z 2. parametru nebylo cislo
    {
        zaklad = NAN;
        *chyba = ERZAKLAD;
    }
    if (*chyb != '\n' && *chyb != 0) //nejaky dalsi znak z 2. parametru neni cislice
    {
        zaklad = NAN;
        *chyba = ERZAKLAD;
    }
    if (zaklad <= 0)
        zaklad = NAN; // pokud je zaklad mensi nez 0, nastavime ho na hodnotu NAN
    else if (zaklad == 1)
        zaklad = NAN;
    else {}

    if (cislo <= 0) // pokud je cislo mensi nebo rovno 0, nastavime ho na hodnotu NAN, protoze logaritmus neni v zapornych cislech definovan
        cislo = NAN;

    vysledek = logax_(zaklad, sigdig, cislo); // do vysledku se mi preda vypocet z funkce logax_
    return vysledek;
}

/** Struktura pro vypocet harmonickeho prumeru.**/
/**
* Do promenne pocet se zapisuje, kolikate cislo je brano ze standardniho vstupu.
* Do promenne suma se postupne zapisuje soucet 1/cisel branych ze standardniho vstupu.
**/
typedef struct mezivysledek
{
    int pocet;
    double suma;
}TMezivysledek;

/**
* Funkce pocitajici harmonicky prumer s vyuzitim struktury prumer typu TMezivysledek,
* ve ktere se, pri kazdem pridani dalsiho cisla, meni jeji jednotlive promenne.
* @param prumer Struktura s pomocnymi potrebnymi k vypoctu harmonickeho prumeru.
* @param *vysledek Ukazatel na predavane cislo, do ktereho se ulozi vysledek.
**/
TMezivysledek harmonickyPrumer(TMezivysledek prumer, double *vysledek)
{

    if (*vysledek == 0 || *vysledek == NAN) // pokud je predane cislo nula nebo nedefinovano, pak je vysledek nedefinovany
    {
        *vysledek = NAN;
        prumer.suma = NAN;
    }
    else if (prumer.suma == 0)
        // jeli suma ze struktury prumer nulova, pak je vysledek stejny jako zadavane cislo,
        // protoze ve vzorecku se deli sumou a nulou nelze delit
    {
        *vysledek = *vysledek;
        prumer.suma = prumer.suma + 1/ *vysledek; // k prubeznemu souctu se pricte 1/zadane cislo
    }
    else // vypocte se harmonicky prumer podle vzorecku s pomoci prubezneho souctu
    {
        prumer.suma = prumer.suma + 1/ *vysledek;
        *vysledek = prumer.pocet / prumer.suma;
    }
    return prumer; // vracim premenenou strukturu a vysledek je ulozen v cislu, na ktery ukazuji ukazatelem vysledek
}

/** Struktura pro vypocet smerodatne odchylky.**/
/**
* Do promenne pocet se zapisuje, kolikate cislo je brano ze standardniho vstupu.
* Do promenne suma se zapisuje postupne soucet cisel branych ze standardniho vstupu.
* Do promenne suma se postupne zapisuje soucet cisel branych ze standardniho vstupu na druhou.
**/
typedef struct meziSmOd
{
    int pocet;
    double suma;
    double suma2;
}TMeziSmOd;

/**
* Funkce pocitajici smerodatnou odchylku s vyuzitim struktury odchylka typu TMeziSmOd,
* ve ktere se, pri kazdem pridani dalsiho cisla, meni jeji jednotlive promenne.
* @param odchylka Struktura s pomocnymi potrebnymi k vypoctu smerodatne odchylky.
* @param *cislo Ukazatel na predavane cislo, do ktereho se ulozi vysledek smerodatne odchylky.
**/
TMeziSmOd smerodatnaOdchylka(TMeziSmOd odchylka, double *cislo)
{
    double prumer;

    odchylka.suma = odchylka.suma + *cislo; // k souctu doposud branych cisel se pricte nasledujici cislo ze vstupu
    odchylka.suma2 = odchylka.suma2 + (*cislo * *cislo);
        // k souctu druhych mocnin branych cisel se pricte druha mocnina nasledujiciho cisla
    prumer = odchylka.suma / odchylka.pocet; // ze souctu a poctu cisel se vypocita aktualni prumerna hodnota

    if (odchylka.pocet > 1) // pokud je pocet cisel vetsi nez 1 (pouze z jednoho nelze vypocitat smerodatna odchylka), provede se vypocet
        *cislo = sqrt((1.0/(odchylka.pocet-1)) * (odchylka.suma2 - 2*prumer*odchylka.suma + odchylka.pocet*prumer*prumer));
    else
        *cislo = 0;

    return odchylka; // vracim premenenou strukturu a vysledek je ulozen v cisle, na ktere ukazuji pomoci ukazatele cislo
}

/**
* Funkce, ktera nam vrati cislo podle toho, ktery parametr uzivatel zadal.
* 1 - parametr -h
* 2 - parametr --powax
* 3 - parametr --logax
* 4 - parametr --hm
* 5 - parametr --stddev
* 6 - chybny 1. parametr
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
*/
int zjistiParam(int argc, char *argv[])
{
    if (argc == 2 && strcmp("-h", argv[1]) == 0) // napoveda
        return 1;
    else if (argc == 4 && strcmp("--powax", argv[1]) == 0) // obecna mnozina
        return 2;
    else if (argc == 4 && strcmp("--logax", argv[1]) == 0) // obecny logaritmus
        return 3;
    else if (argc == 2 && strcmp("--hm", argv[1]) == 0) // harmonicky prumer
        return 4;
    else if (argc == 2 && strcmp("--stddev", argv[1]) == 0) // smerodatna odchylka
        return 5;
    else return 6; // chybny parametr
}

/**
* Hlavni funkce.
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
**/
int main(int argc, char *argv[])
{
    int chyba = EROK;
    int overeni = 0; // budu pouzivat pro overeni vstupu pomoci scanf
    double cislo; // do teto promenne se zapise cislo ze standardniho vstupu
    int param = zjistiParam(argc, argv); // do param se zapise cislo, podle toho jaky parametr byl zadan
    int pruchod = 0; // pocitadlo - kolikrat se nacetlo cislo ze standardniho vstupu
    TMezivysledek prumer = {0, 0}; // nastaveni vsech clenu struktury na nulu
    TMeziSmOd odchylka = {0, 0, 0}; // nastaveni vsech clenu struktury na nulu

    if (param == 1)
    {
        napoveda(); // vypise se napoveda a program skonci
        chyba = EROK;
    }
    else if (param == 6) // vypise se chyba (chybny parametr) a program skonci
        chyba = ERPARAM;
    else
    {
        while ((overeni = scanf("%lf", &cislo)) != EOF)
        {
            pruchod ++;
            if (overeni != 1)
            { // zjistuji, zda bylo zadane cislo typu double - pokud ne, nastavi se promenna cislo na konstantu NAN
                scanf("%*s");
                cislo = NAN;
            }
            if ((fabs(cislo)) > DBL_DIG)
            {
                cislo = INFINITY;
                printf("%.10e\n", cislo);
            }
            else
            {
                switch(param) // podle parametru vybiram jednotlive operace se vstupem
                    {
                        case 2: cislo = powax(argv, cislo, &chyba); // vypocita se obecna mnozina
                                if (chyba != EROK && pruchod == 1)
                                // pokud pri vypoctu nastala chyba (je pozadovana vetsi presnost nez je mozne),
                                // vypise se chybova hlaska, ale pokracuje se ve vypoctu s nejvetsi moznou presnosti
                                    vypisChybu(chyba);
                                break;
                        case 3: cislo = logax(argv, cislo, &chyba); // vypocita se obecny logaritmus
                                if (chyba != EROK && pruchod == 1)
                                // pokud pri vypoctu nastala chyba (je pozadovana vetsi presnost nez je mozne),
                                // vypise se chybova hlaska, ale pokracuje se ve vypoctu s nejvetsi moznou presnosti
                                    vypisChybu(chyba);
                                break;
                        case 4: prumer.pocet ++; // kolikate cislo na vstupu uz bylo prijato - nutno pro vypocet
                                prumer = harmonickyPrumer(prumer, &cislo); // vypocita se harmonicky prumer
                                break;
                        case 5: odchylka.pocet ++; // kolikate cislo na vstupu uz bylo prijato - nutno pro vypocet
                                odchylka = smerodatnaOdchylka(odchylka, &cislo); // vypocita se smerodatna odchylka
                                break;
                        default: chyba = ERPARAM; break; // zaznamena chybu (chybny parametr)
                    }
                if (chyba == ERSIGMIN || chyba == ERSIG || chyba == ERZAKLAD) // pokud je pozadovana mensi presnost nez nula, vypise se chyba a program skonci
                    return EXIT_FAILURE;
                else
                {
                    chyba = EROK;
                    printf("%.10e\n", cislo); // vytiskne vysledek v pozadovanem tvaru na standardni vystup
                }
            }
        }
    }

    if (chyba != EROK) //kontrola chybove hlasky a pripadne jeji vypsani, pokud se behem programu nejaka chyba vyskytla
    {
        vypisChybu(chyba);
        return EXIT_FAILURE;
    }
    else return EXIT_SUCCESS;
}
