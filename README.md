# README #

Second programming task for a course Introduction to Programming Systems:
Program process an arbitrarily long sequence of number values of type double from standard input by using iterative algorithms based on input parameters.

Compile project by command

```
#!c

make
```